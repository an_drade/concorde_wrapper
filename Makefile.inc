###############################################################################
# Author: Carlos Eduardo de Andrade <andrade@ic.unicamp.br>
#
# (c) Copyright 2012 Institute of Computing, University of Campinas.
#     All Rights Reserved.
#
#  Created on : Jun 07, 2012 by andrade
#  Last update: Jul 17, 2012 by andrade
#
# This software is licensed under the Common Public License. Please see
# accompanying file for terms.
###############################################################################

###############################################################################
# User Flags
###############################################################################

# Compiler flags for debugging
USER_FLAGS = #-g3 -fexceptions

# Compiler flags for performace
# Machine type
MACHINE = core2
ARCH = 32
#ARCH = 64

# Flags
USER_FLAGS += -Wall -Wextra -pedantic -O3 -march=$(MACHINE) -mtune=$(MACHINE) -m$(ARCH) \
	-pthread -fopenmp \
	-funroll-loops -fpeel-loops -fprefetch-loop-arrays \
	-fomit-frame-pointer -ftracer \
    #-mfpmath=sse,387 

#LDFLAGS = -fdata-sections -ffunction-sections

###############################################################################
# Common libs
###############################################################################

LIBS = -lm -pthread -fopenmp

###############################################################################
# Compiler and linker defs
###############################################################################

# C Compiler command and flags
CC = gcc
CFLAGS = $(USER_FLAGS)

# C++ Compiler command and flags
CXX = g++
CXXFLAGS = $(USER_FLAGS)

# Lib maker commands
AR = ar
ARFLAGS	= rv
RANLIB = ranlib

# Other includes
RM = rm
SHELL = /bin/bash
