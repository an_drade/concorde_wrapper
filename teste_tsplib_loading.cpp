/******************************************************************************
 * teste.cpp: Main Process.
 *
 * Author: Carlos Eduardo de Andrade <andrade@ic.unicamp.br>
 *
 * (c) Copyright 2012 Institute of Computing, University of Campinas.
 *     All Rights Reserved.
 *
 *  Created on : Jun 16, 2012 by andrade
 *  Last update: Jun 16, 2012 by andrade
 *
 * This software is licensed under the Common Public License. Please see
 * accompanying file for terms.
 *****************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <limits>
#include <cstdlib>
#include <vector>
#include <boost/timer.hpp>
#include <boost/lexical_cast.hpp>
#include <lemon/full_graph.h>
#include <lemon/lgf_writer.h>
#include <lemon/dim2.h>

using namespace std;
using namespace lemon;

#include "concorde_wrapper.hpp"

double printCycle(const vector<FullGraph::Node> &cycle, const FullGraph &graph,
                  const FullGraph::EdgeMap<int> &dist);

//-------------------------------[ Main ]------------------------------------//

int main(int argc, char* argv[]) {
    if(argc < 2) {
        cerr << "> Passe o numero de vertices" << endl;
        return 1;
    }

    FullGraph graph;
    FullGraph::EdgeMap<int> dist(graph);
    FullGraph::NodeMap< dim2::Point<double> > coords(graph);
    bool loaded_coords;

    try {
        const unsigned long SEED = 152269023;
        const unsigned long MAX_THR = 2;

        ConcordeWrapper concorde(SEED, MAX_THR);
        concorde.loadTSP(argv[2], &graph, &dist, &coords, &loaded_coords);
        concorde.updateData(&graph, &dist);

        if(loaded_coords)
            for(FullGraph::NodeIt it(graph); it != INVALID; ++it)
                cout << "\n> " << graph.id(it) << ": "<< coords[it];
        else
            cout << "\nWe couldn't load the geometric information" << endl;

        vector<FullGraph::Node> nodes;
        vector<FullGraph::Node> *cycle = new vector<FullGraph::Node>();

        for(FullGraph::NodeIt it(graph); it != INVALID; ++it)
            nodes.push_back(it);

        concorde.getLinKernTour(nodes, cycle);

        double cost = 0.0;
        cout << "\n>> Cycle: ";
        cost = printCycle(nodes, graph, dist);
        cout << "\n|| Cost: " << cost << endl;

        delete cycle;
    }
    catch(exception& e) {
        cerr << "\n***********************************************************"
             << "\n****  Exception Occured: " << e.what()
             << "\n***********************************************************"
             << endl;
        return 70; // BSD software internal error code
    }
    return 0;
}

//----------------------------[ Print Cycle ]---------------------------------//

double printCycle(const vector<FullGraph::Node> &cycle, const FullGraph &graph,
                  const FullGraph::EdgeMap<int> &dist) {
    double cost = 0.0;
    vector<FullGraph::Node>::const_iterator it1, it2;

    for(it2 = cycle.begin(); it2 != cycle.end(); ) {
        it1 = it2++;
        cout << graph.id(*it1) << " ";

        if(it2 != cycle.end())
            cost += dist[graph.edge(*it1,*it2)];
    }
    cost += dist[graph.edge(*it1,*(cycle.begin()))];

    return cost;
}
