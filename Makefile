###############################################################################
# Author: Carlos Eduardo de Andrade <andrade@ic.unicamp.br>
#
# (c) Copyright 2012 Institute of Computing, University of Campinas.
#     All Rights Reserved.
#
#  Created on : Jul 09, 2011 by andrade
#  Last update: Jul 31, 2012 by andrade
#
# This software is licensed under the Common Public License. Please see
# accompanying file for terms.
###############################################################################

###############################################################################
# User Defines
###############################################################################

# Set debug mode
USER_DEFINES += #-DDEBUG -DFULLDEBUG

include Makefile.inc

###############################################################################
# Build options
###############################################################################

EXE = teste

###############################################################################
# The user source files
###############################################################################

# Include dir
USER_INCDIRS = . \
	./concorde/INCLUDE \
	./concorde_wrapper

# Source directories (to clean up object files)
SRC_DIRS = . \
	./concorde_wrapper

# Object files
OBJS = \
	./concorde_wrapper/concorde_wrapper.o

# Main function
OBJS += ./teste_tsplib_loading.o

###############################################################################
# Lib and include definitions
###############################################################################

##############################
# Concorde libs
##############################

CONCORDE_PATH = ./concorde
CONCORDE_LIBS = \
	$(CONCORDE_PATH)/LINKERN/linkern.a \
	$(CONCORDE_PATH)/EDGEGEN/edgegen.a \
	$(CONCORDE_PATH)/KDTREE/kdtree.a \
	$(CONCORDE_PATH)/FMATCH/fmatch.a \
	$(CONCORDE_PATH)/UTIL/util.a

##############################
# Consolidate paths
##############################

# Consolidate include paths
USER_INCDIRS += #\
	#$(CPLEXINCDIRS)
	
# Consolidate include paths
USER_LIBDIRS += #\
	#$(CPLEXLIBDIRS)

# Libraries necessary to link.
LIBS += -lemon
	
# Compiler flags
USER_FLAGS += 

###############################################################################
# Compiler defs
###############################################################################

# C++ Compiler command
CXX = g++

# C++ Compiler options
CXXFLAGS = $(USER_FLAGS)

# Necessary Include dirs
# Put -I in front of dirs
INCLUDES = `for i in $(USER_INCDIRS); do echo $$i | sed -e 's/^/-I/'; done`

# Necessary library dirs
# Put -L in front of dirs
LIBDIRS = `for i in $(USER_LIBDIRS); do echo $$i | sed -e 's/^/-L/'; done`

###############################################################################
# Build Rules
###############################################################################

all: $(EXE)

.PHONY: all concorde strip clean doc docclean depclean
.SUFFIXES: .cpp .o

$(EXE): concorde $(OBJS)
	@echo "--> Linking objects... "
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(OBJS) $(CONCORDE_LIBS) $(MAURO_LIB) $(LIBDIRS) $(LIBS) -o $@
	@echo

.cpp.o:
	@echo "--> Compiling $<..."
	$(CXX) $(CXXFLAGS) $(INCLUDES) $(USER_DEFINES) -c $< -o $@
	@echo
	
concorde:
	@echo "|---------------------[ Compiling Concorde ]---------------------|"
	make -C concorde
	@echo "|-----------------[ End of compiling Concorde ]------------------|"

strip: $(EXE)
	@echo "-->Stripping $<..."
	strip $<
	@echo

doc:
	doxygen Doxyfile

clean:
	@echo "--> Cleaning compiled..."
	rm -rf $(EXE) $(OBJS) $(CLIQUER_OBJS)
	
depclean: clean docclean
	rm -rf `for i in $(SRC_DIRS); do echo $$i*~; done` 
	rm -rf Debug
	make -C concorde clean
		
docclean:
	@echo "--> Cleaning doc..."
	rm -rf doc
